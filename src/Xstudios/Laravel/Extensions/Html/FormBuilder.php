<?php

namespace Xstudios\Laravel\Extensions\Html;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Form;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;

class FormBuilder extends \Illuminate\Html\FormBuilder
{

	// ------------------------------------------------------------------------

	public function stateList()
	{
		return array(
			''   => "Select a State",
			'AL' => "Alabama",
			'AK' => "Alaska",
			'AZ' => "Arizona",
			'AR' => "Arkansas",
			'CA' => "California",
			'CO' => "Colorado",
			'CT' => "Connecticut",
			'DE' => "Delaware",
			'DC' => "District Of Columbia",
			'FL' => "Florida",
			'GA' => "Georgia",
			'HI' => "Hawaii",
			'ID' => "Idaho",
			'IL' => "Illinois",
			'IN' => "Indiana",
			'IA' => "Iowa",
			'KS' => "Kansas",
			'KY' => "Kentucky",
			'LA' => "Louisiana",
			'ME' => "Maine",
			'MD' => "Maryland",
			'MA' => "Massachusetts",
			'MI' => "Michigan",
			'MN' => "Minnesota",
			'MS' => "Mississippi",
			'MO' => "Missouri",
			'MT' => "Montana",
			'NE' => "Nebraska",
			'NV' => "Nevada",
			'NH' => "New Hampshire",
			'NJ' => "New Jersey",
			'NM' => "New Mexico",
			'NY' => "New York",
			'NC' => "North Carolina",
			'ND' => "North Dakota",
			'OH' => "Ohio",
			'OK' => "Oklahoma",
			'OR' => "Oregon",
			'PA' => "Pennsylvania",
			'RI' => "Rhode Island",
			'SC' => "South Carolina",
			'SD' => "South Dakota",
			'TN' => "Tennessee",
			'TX' => "Texas",
			'UT' => "Utah",
			'VT' => "Vermont",
			'VA' => "Virginia",
			'WA' => "Washington",
			'WV' => "West Virginia",
			'WI' => "Wisconsin",
			'WY' => "Wyoming"
		);
	}

	// ------------------------------------------------------------------------

	public function countryList()
	{
		return array(
			''   => "Select a Country",
			"AF" => "Afghanistan",
			"AX" => "Åland Islands",
			"AL" => "Albania",
			"DZ" => "Algeria",
			"AS" => "American Samoa",
			"AD" => "Andorra",
			"AO" => "Angola",
			"AI" => "Anguilla",
			"AQ" => "Antarctica",
			"AG" => "Antigua and Barbuda",
			"AR" => "Argentina",
			"AM" => "Armenia",
			"AW" => "Aruba",
			"AU" => "Australia",
			"AT" => "Austria",
			"AZ" => "Azerbaijan",
			"BS" => "Bahamas",
			"BH" => "Bahrain",
			"BD" => "Bangladesh",
			"BB" => "Barbados",
			"BY" => "Belarus",
			"BE" => "Belgium",
			"BZ" => "Belize",
			"BJ" => "Benin",
			"BM" => "Bermuda",
			"BT" => "Bhutan",
			"BO" => "Bolivia, Plurinational State of",
			"BQ" => "Bonaire, Sint Eustatius and Saba",
			"BA" => "Bosnia and Herzegovina",
			"BW" => "Botswana",
			"BV" => "Bouvet Island",
			"BR" => "Brazil",
			"IO" => "British Indian Ocean Territory",
			"BN" => "Brunei Darussalam",
			"BG" => "Bulgaria",
			"BF" => "Burkina Faso",
			"BI" => "Burundi",
			"KH" => "Cambodia",
			"CM" => "Cameroon",
			"CA" => "Canada",
			"CV" => "Cape Verde",
			"KY" => "Cayman Islands",
			"CF" => "Central African Republic",
			"TD" => "Chad",
			"CL" => "Chile",
			"CN" => "China",
			"CX" => "Christmas Island",
			"CC" => "Cocos (Keeling) Islands",
			"CO" => "Colombia",
			"KM" => "Comoros",
			"CG" => "Congo",
			"CD" => "Congo (the Democratic Republic of the)",
			"CK" => "Cook Islands",
			"CR" => "Costa Rica",
			"HR" => "Croatia",
			"CU" => "Cuba",
			"CW" => "Curaçao",
			"CY" => "Cyprus",
			"CZ" => "Czech Republic",
			"CI" => "Côte d'Ivoire",
			"DK" => "Denmark",
			"DJ" => "Djibouti",
			"DM" => "Dominica",
			"DO" => "Dominican Republic",
			"EC" => "Ecuador",
			"EG" => "Egypt",
			"SV" => "El Salvador",
			"GQ" => "Equatorial Guinea",
			"ER" => "Eritrea",
			"EE" => "Estonia",
			"ET" => "Ethiopia",
			"FK" => "Falkland Islands  [Malvinas]",
			"FO" => "Faroe Islands",
			"FJ" => "Fiji",
			"FI" => "Finland",
			"FR" => "France",
			"GF" => "French Guiana",
			"PF" => "French Polynesia",
			"TF" => "French Southern Territories",
			"GA" => "Gabon",
			"GM" => "Gambia (The)",
			"GE" => "Georgia",
			"DE" => "Germany",
			"GH" => "Ghana",
			"GI" => "Gibraltar",
			"GR" => "Greece",
			"GL" => "Greenland",
			"GD" => "Grenada",
			"GP" => "Guadeloupe",
			"GU" => "Guam",
			"GT" => "Guatemala",
			"GG" => "Guernsey",
			"GN" => "Guinea",
			"GW" => "Guinea-Bissau",
			"GY" => "Guyana",
			"HT" => "Haiti",
			"HM" => "Heard Island and McDonald Islands",
			"VA" => "Holy See  [Vatican City State]",
			"HN" => "Honduras",
			"HK" => "Hong Kong",
			"HU" => "Hungary",
			"IS" => "Iceland",
			"IN" => "India",
			"ID" => "Indonesia",
			"IR" => "Iran (the Islamic Republic of)",
			"IQ" => "Iraq",
			"IE" => "Ireland",
			"IM" => "Isle of Man",
			"IL" => "Israel",
			"IT" => "Italy",
			"JM" => "Jamaica",
			"JP" => "Japan",
			"JE" => "Jersey",
			"JO" => "Jordan",
			"KZ" => "Kazakhstan",
			"KE" => "Kenya",
			"KI" => "Kiribati",
			"KP" => "Korea (the Democratic People's Republic of)",
			"KR" => "Korea (the Republic of)",
			"KW" => "Kuwait",
			"KG" => "Kyrgyzstan",
			"LA" => "Lao People's Democratic Republic",
			"LV" => "Latvia",
			"LB" => "Lebanon",
			"LS" => "Lesotho",
			"LR" => "Liberia",
			"LY" => "Libya",
			"LI" => "Liechtenstein",
			"LT" => "Lithuania",
			"LU" => "Luxembourg",
			"MO" => "Macao",
			"MK" => "Macedonia (the former Yugoslav Republic of)",
			"MG" => "Madagascar",
			"MW" => "Malawi",
			"MY" => "Malaysia",
			"MV" => "Maldives",
			"ML" => "Mali",
			"MT" => "Malta",
			"MH" => "Marshall Islands",
			"MQ" => "Martinique",
			"MR" => "Mauritania",
			"MU" => "Mauritius",
			"YT" => "Mayotte",
			"MX" => "Mexico",
			"FM" => "Micronesia (the Federated States of)",
			"MD" => "Moldova (the Republic of)",
			"MC" => "Monaco",
			"MN" => "Mongolia",
			"ME" => "Montenegro",
			"MS" => "Montserrat",
			"MA" => "Morocco",
			"MZ" => "Mozambique",
			"MM" => "Myanmar",
			"NA" => "Namibia",
			"NR" => "Nauru",
			"NP" => "Nepal",
			"NL" => "Netherlands",
			"NC" => "New Caledonia",
			"NZ" => "New Zealand",
			"NI" => "Nicaragua",
			"NE" => "Niger",
			"NG" => "Nigeria",
			"NU" => "Niue",
			"NF" => "Norfolk Island",
			"MP" => "Northern Mariana Islands",
			"NO" => "Norway",
			"OM" => "Oman",
			"PK" => "Pakistan",
			"PW" => "Palau",
			"PS" => "Palestine, State of",
			"PA" => "Panama",
			"PG" => "Papua New Guinea",
			"PY" => "Paraguay",
			"PE" => "Peru",
			"PH" => "Philippines",
			"PN" => "Pitcairn",
			"PL" => "Poland",
			"PT" => "Portugal",
			"PR" => "Puerto Rico",
			"QA" => "Qatar",
			"RO" => "Romania",
			"RU" => "Russian Federation",
			"RW" => "Rwanda",
			"RE" => "Réunion",
			"BL" => "Saint Barthélemy",
			"SH" => "Saint Helena, Ascension and Tristan da Cunha",
			"KN" => "Saint Kitts and Nevis",
			"LC" => "Saint Lucia",
			"MF" => "Saint Martin (French part)",
			"PM" => "Saint Pierre and Miquelon",
			"VC" => "Saint Vincent and the Grenadines",
			"WS" => "Samoa",
			"SM" => "San Marino",
			"ST" => "Sao Tome and Principe",
			"SA" => "Saudi Arabia",
			"SN" => "Senegal",
			"RS" => "Serbia",
			"SC" => "Seychelles",
			"SL" => "Sierra Leone",
			"SG" => "Singapore",
			"SX" => "Sint Maarten (Dutch part)",
			"SK" => "Slovakia",
			"SI" => "Slovenia",
			"SB" => "Solomon Islands",
			"SO" => "Somalia",
			"ZA" => "South Africa",
			"GS" => "South Georgia and the South Sandwich Islands",
			"SS" => "South Sudan",
			"ES" => "Spain",
			"LK" => "Sri Lanka",
			"SD" => "Sudan",
			"SR" => "Suriname",
			"SJ" => "Svalbard and Jan Mayen",
			"SZ" => "Swaziland",
			"SE" => "Sweden",
			"CH" => "Switzerland",
			"SY" => "Syrian Arab Republic",
			"TW" => "Taiwan (Province of China)",
			"TJ" => "Tajikistan",
			"TZ" => "Tanzania, United Republic of",
			"TH" => "Thailand",
			"TL" => "Timor-Leste",
			"TG" => "Togo",
			"TK" => "Tokelau",
			"TO" => "Tonga",
			"TT" => "Trinidad and Tobago",
			"TN" => "Tunisia",
			"TR" => "Turkey",
			"TM" => "Turkmenistan",
			"TC" => "Turks and Caicos Islands",
			"TV" => "Tuvalu",
			"UG" => "Uganda",
			"UA" => "Ukraine",
			"AE" => "United Arab Emirates",
			"GB" => "United Kingdom",
			"US" => "United States",
			"UM" => "United States Minor Outlying Islands",
			"UY" => "Uruguay",
			"UZ" => "Uzbekistan",
			"VU" => "Vanuatu",
			"VE" => "Venezuela, Bolivarian Republic of",
			"VN" => "Viet Nam",
			"VG" => "Virgin Islands (British)",
			"VI" => "Virgin Islands (U.S.)",
			"WF" => "Wallis and Futuna",
			"EH" => "Western Sahara",
			"YE" => "Yemen",
			"ZM" => "Zambia",
			"ZW" => "Zimbabwe"
		);
	}

	// ------------------------------------------------------------------------

	public function selectState($name="state", $selected=null, $options=array())
	{
		$states = FormBuilder::stateList();

		return FormBuilder::select($name, $states, $selected, $options);
	}

	// ------------------------------------------------------------------------

	public function selectCountry($name="country", $selected=null, $options=array())
	{
		$countries = FormBuilder::countryList();

		return FormBuilder::select($name, $countries, $selected, $options);
	}

	// ------------------------------------------------------------------------
	// BOOTSTRAP MARKUP
	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 static control markup.
	 *
	 * @param  string $label
	 * @param  string $value
	 * @return string
	 */
	public function bootstrapStaticControl($label, $value)
	{
		$html ='
		<div class="form-group">
			<label class="col-sm-2 control-label">%s</label>
			<div class="col-sm-10">
				<p class="form-control-static">%s</p>
			</div>
		</div>
		';

		return sprintf($html, $label, $value);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 readonly field markup.
	 *
	 * @param  string $label
	 * @param  string $value
	 * @param  array $attr
	 * @return string
	 */
	public function bootstrapReadonlyTextControl($label, $value, $attr=array())
	{
		$html ='
		<div class="form-group">
			%s
			<div class="col-sm-10">
				%s
			</div>
		</div>
		';

		$attributes = array('class' => 'form-control', 'readonly');
		$attributes = array_merge($attributes, $attr);

		$label = Form::label(null, $label, array('class'=>'col-sm-2'));
		$input = Form::text(null, $value, $attributes);

		return sprintf($html, $label, $input);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 disabled field markup.
	 *
	 * @param  string $label
	 * @param  string $value
	 * @param  array $attr
	 * @return string
	 */
	public function bootstrapDisabledTextControl($label, $value, $attr=array())
	{
		$html ='
		<div class="form-group">
			%s
			<div class="col-sm-10">
				%s
			</div>
		</div>
		';

		$attributes = array('class' => 'form-control', 'disabled');
		$attributes = array_merge($attributes, $attr);

		$label = Form::label(null, $label, array('class'=>'col-sm-2'));
		$input = Form::text(null, $value, $attributes);

		return sprintf($html, $label, $input);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 text control markup.
	 *
	 * @param  string $label
	 * @param  string $name
	 * @param  array $attr
	 * @return string
	 */
	public function bootstrapTextControl($label, $name, $attr=array())
	{
		$html ='
		<div class="form-group">
			%s
			<div class="col-sm-10">
				%s
			</div>
		</div>
		';

		$attributes = array('class' => 'form-control');
		$attributes = array_merge($attributes, $attr);

		$label = Form::label($name, $label, array('class'=>'col-sm-2'));
		$input = Form::text($name, Input::old($name), $attributes);

		return sprintf($html, $label, $input);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 password text control markup.
	 *
	 * @param  string $label
	 * @param  string $name
	 * @return string
	 */
	public function bootstrapPasswordControl($label, $name)
	{
		$html ='
		<div class="form-group">
			%s
			<div class="col-sm-10">
				%s
			</div>
		</div>
		';

		$label = Form::label($name, $label, array('class'=>'col-sm-2'));
		$input = Form::password($name, array('class' => 'form-control'));

		return sprintf($html, $label, $input);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 textarea control markup.
	 *
	 * @param  string $label
	 * @param  string $name
	 * @param  array $attr
	 * @return string
	 */
	public function bootstrapTextAreaControl($label, $name, $attr=array())
	{
		$html ='
		<div class="form-group">
			%s
			<div class="col-sm-10">
				%s
			</div>
		</div>
		';

		$attributes = array('class' => 'form-control');
		$attributes = array_merge($attributes, $attr);

		$label = Form::label($name, $label, array('class'=>'col-sm-2'));
		$input = Form::textarea($name, Input::old($name), $attributes);

		return sprintf($html, $label, $input);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 select control markup.
	 *
	 * @param  string $label
	 * @param  string $name
	 * @param  array $options
	 * @param  array $attr
	 * @return string
	 */
	public function bootstrapSelectControl($label, $name, $options, $attr=array())
	{
		$html ='
		<div class="form-group">
			%s
			<div class="col-sm-10">
				%s
			</div>
		</div>
		';

		$attributes = array('class' => 'form-control');
		$attributes = array_merge($attributes, $attr);

		$label = Form::label($name, $label, array('class'=>'col-sm-2'));
		$input = Form::select($name, $options, Input::old($name), $attributes);

		return sprintf($html, $label, $input);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 select control markup.
	 *
	 * @param  string $label
	 * @param  string $name
	 * @param  string $value
	 * @param  string $help_text
	 * @param  array $attr
	 * @return string
	 */
	public function bootstrapCheckboxControl($label, $name, $value, $checked=false, $help_text='', $attr=array())
	{
		$html ='
		<div class="form-group">
			<div class="col-sm-2"></div>
			<div class="col-sm-10">
				<div class="checkbox">
					<label>
						%s %s
					</label>
					<p class="help-block">%s</p>
				</div>
			</div>
		</div>
		';

		$attributes = array();

		// Passing true as the 3rd param should check the checkbox,
		// however it does not, so we implement a fix
		if ($checked)
		{
			$attributes = array('checked' => 'checked');
		}

		$attributes = array_merge($attributes, $attr);

		$input = Form::checkbox($name, $value, false, $attributes);

		return sprintf($html, $input, $label, $help_text);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 file input markup.
	 * NOTE: Requires http://jasny.github.io/bootstrap/javascript/#fileinput
	 *
	 * @param  string $label
	 * @param  string $name
	 * @param  string $value
	 * @return string
	 */
	public function bootstrapFileInput($label, $name, $value='')
	{
		$html = '
		<div class="form-group">
			%s
			<div class="col-sm-10">
				<div class="fileinput fileinput-new input-group" data-provides="fileinput">
					<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename">%s</span></div>
					<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="%s"></span>
					<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
				</div>
			</div>
		</div>
		';

		$label = Form::label($name, $label, array('class'=>'col-sm-2'));

		return sprintf($html, $label, $value, $name);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 datepicker control markup.
	 *
	 * @param  string $label
	 * @param  string $name
	 * @param  string $date_format (eg - yyyy-mm-dd)
	 * @return string
	 */
	public function bootstrapDatepicker($label, $name, $date_format='yyyy-mm-dd')
	{
		$html ='
		<div class="form-group">
			%s
			<div class="col-sm-10">
				%s
			</div>
		</div>
		';

		$label = Form::label($name, $label, array('class'=>'col-sm-2'));
		$input = Form::text($name, Input::old($name), array(
			'class'            => 'form-control datepicker',
			'data-date-format' => $date_format
		));

		return sprintf($html, $label, $input);
	}

	// ------------------------------------------------------------------------

	/**
	 * Returns bootstrap 3 save buttons (Cancel / Save).
	 *
	 * @param  string $route
	 * @param  boolean $new  show 'Save and Add New' button
	 * @param  boolean $continue  show 'Save and Continue Editing' button
	 * @return string
	 */
	public function bootstrapSaveButtons($route, $new=true, $continue=true)
	{
		// Determine URL we need for cancel button
		$url = URL::previous() != URL::current() ? URL::previous() : URL::route($route);

		$html ='
		<!--<div class="pull-left">
			<a href="%s" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
		</div>-->

		<div class="pull-right">';
			if ($new) {
				$html .= '<button type="submit" name="save_and_new" value="1" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span> Save and Add New</button>'.PHP_EOL;
			}
			if ($continue) {
				$html .= '<button type="submit" name="save_and_continue" value="1" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Save and Continue Editing</button>'.PHP_EOL;
			}
			$html .= '<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Save</button>
		</div>
		';

		return sprintf($html, $url);
	}

	// ------------------------------------------------------------------------

}
