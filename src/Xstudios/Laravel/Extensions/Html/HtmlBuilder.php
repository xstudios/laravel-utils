<?php

namespace Xstudios\Laravel\Extensions\Html;

use Illuminate\Support\Facades\Request;

class HtmlBuilder extends \Illuminate\Html\HtmlBuilder
{

	// ------------------------------------------------------------------------

	/**
	 * Tests a route against the current URL for active state
	 * If true, returns 'selected' for class name
	 * Usage: HTML::activeState('named.route')
	 */
	public function activeState($route, $class="active")
	{
		return strpos(Request::url(), route($route)) !== false ? $class : '';
	}

	// ------------------------------------------------------------------------

}
