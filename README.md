# X Studios Laravel Utils
#### Author: Tim Santor <tsantor@xstudios.agency>

## Installation
Open your `composer.json` file and add the following to the `repositories` array:

    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/xstudios/laravel-utils.git"
        }
    ],

The add the folllowing to the `require` object:

    "xstudios/laravel-utils": "v0.1.4"

Or to stay on the bleeding edge (stability not guaranteed):

    "xstudios/laravel-utils": "dev-master"

### Install the dependencies

    php composer install

or

    php composer update

Once the package is installed you need to register the service provider with the application. Open up `app/config/app.php` and find the `providers` key.

    'providers' => array(
        # Other providers here ...
        'Xstudios\Laravel\Extensions\Html\HtmlServiceProvider',
    )

## Contribute
In lieu of a formal styleguide, take care to maintain the existing coding style.

## License
MIT License (c) [X Studios](http://xstudios.agency)
